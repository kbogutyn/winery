<?php

namespace Winery\Tests\Sort;

use App\Winery\Sort\PercentageDescSort;
use PHPUnit\Framework\TestCase;

class PercentageDescSortTest extends TestCase
{
    /**
     * @dataProvider dataToSort
     */
    public function test_sort_percentage_desc(array $data, array $expected): void
    {
        $sorter = new PercentageDescSort();
        $sortedArray = $sorter->sort($data);

        $this->assertEquals($expected, $sortedArray);
    }

    public function dataToSort(): array
    {
        return [
            [
                [
                    ['grape' => 'Malbec', 'percentage' => 10],
                    ['grape' => 'Cabernet Sauvignon', 'percentage' => 20],
                    ['grape' => 'Cabernet Franc', 'percentage' => 20],
                ],
                [
                    ['grape' => 'Cabernet Franc', 'percentage' => 20],
                    ['grape' => 'Cabernet Sauvignon', 'percentage' => 20],
                    ['grape' => 'Malbec', 'percentage' => 10],
                ],
            ],
            [
                [
                    ['grape' => 'Malbec', 'percentage' => 40],
                    ['grape' => 'Cabernet Sauvignon', 'percentage' => 10],
                    ['grape' => 'Merlot', 'percentage' => 40],
                    ['grape' => 'Cabernet Franc', 'percentage' => 10],
                ],
                [
                    ['grape' => 'Malbec', 'percentage' => 40],
                    ['grape' => 'Merlot', 'percentage' => 40],
                    ['grape' => 'Cabernet Franc', 'percentage' => 10],
                    ['grape' => 'Cabernet Sauvignon', 'percentage' => 10],
                ],
            ],
            [
                [
                    ['grape' => 'Malbec', 'percentage' => 10],
                    ['grape' => 'Merlot', 'percentage' => 70],
                    ['grape' => 'Cabernet Sauvignon', 'percentage' => 10],
                    ['grape' => 'Cabernet Franc', 'percentage' => 10],
                ],
                [
                    ['grape' => 'Merlot', 'percentage' => 70],
                    ['grape' => 'Cabernet Franc', 'percentage' => 10],
                    ['grape' => 'Cabernet Sauvignon', 'percentage' => 10],
                    ['grape' => 'Malbec', 'percentage' => 10],
                ],
            ],
        ];
    }
}
