<?php

namespace Winery\Tests\Label;

use App\Winery\Grape\CabernetFranc;
use App\Winery\Grape\CabernetSauvignon;
use App\Winery\Grape\GrapeInterface;
use App\Winery\Grape\Malbec;
use App\Winery\Grape\Merlot;
use App\Winery\Label\LabelBuilder\LabelBuilder;
use App\Winery\Label\LabelDirector\LabelDirector;
use App\Winery\Sort\PercentageDescSort;
use PHPUnit\Framework\TestCase;

class LabelDirectorTest extends TestCase
{
    private $labelDirector;

    public function setUp(): void
    {
        parent::setUp();

        $this->labelDirector = new LabelDirector(new LabelBuilder(), new PercentageDescSort());
    }

    public function test_create_valid_label(): void
    {
        $grape4 = new CabernetSauvignon();
        $grape1 = new CabernetFranc();
        $grape2 = new Merlot();
        $grape3 = new Malbec();

        $this->labelDirector->addGrapeWithPercentage($grape1, 10);
        $this->labelDirector->addGrapeWithPercentage($grape2, 70);
        $this->labelDirector->addGrapeWithPercentage($grape3, 10);
        $this->labelDirector->addGrapeWithPercentage($grape4, 10);

        $this->labelDirector->buildLabel();
        $label = $this->labelDirector->getLabel()->getText();

        $this->assertEquals('Merlot Cabernet Franc Cabernet Sauvignon Malbec', $label);
    }

    /**
     * @expectedException \App\Winery\Exception\LabelToLongException
     */
    public function test_attempt_to_create_label_with_length_greater_than_50_throws_exception(): void
    {
        $grape = new GrapeWithLongName();

        $this->labelDirector->addGrapeWithPercentage($grape, 100);
        $this->labelDirector->buildLabel();
    }

    /**
     * @expectedException \App\Winery\Exception\ToManyGrapesException
     */
    public function test_attempt_to_create_label_from_more_than_four_grapes_throws_exception(): void
    {
        $grape1 = new CabernetFranc();
        $grape2 = new Merlot();
        $grape3 = new Malbec();
        $grape4 = new CabernetSauvignon();
        $grape5 = new TestGrape();

        $this->labelDirector->addGrapeWithPercentage($grape1, 10);
        $this->labelDirector->addGrapeWithPercentage($grape2, 60);
        $this->labelDirector->addGrapeWithPercentage($grape3, 10);
        $this->labelDirector->addGrapeWithPercentage($grape4, 10);
        $this->labelDirector->addGrapeWithPercentage($grape5, 10);
    }

    /**
     * @expectedException \App\Winery\Exception\TotalGrapesPercentageException
     */
    public function test_attempt_to_create_label_with_total_grapes_percentage_over_one_hundred_percent_throws_exception(): void
    {
        $grape1 = new CabernetFranc();
        $grape2 = new Merlot();

        $this->labelDirector->addGrapeWithPercentage($grape1, 50);
        $this->labelDirector->addGrapeWithPercentage($grape2, 60);
    }
}

class TestGrape implements GrapeInterface
{
    public function getName(): string
    {
        return 'Test Grape ';
    }
}

class GrapeWithLongName implements GrapeInterface
{
    public function getName(): string
    {
        return str_repeat('a', 51);
    }
}
