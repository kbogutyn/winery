<?php

namespace Winery\Tests\Label;

use App\Winery\Label\LabelBuilder\LabelBuilder;
use PHPUnit\Framework\TestCase;

class LabelBuilderTest extends TestCase
{
    private $labelBuilder;

    public function setUp(): void
    {
        parent::setUp();

        $this->labelBuilder = new LabelBuilder();
    }

    public function test_get_empty_label(): void
    {
        $label = $this->labelBuilder->getLabel();

        $this->assertEmpty($label->getText());
    }

    public function test_get_label_text(): void
    {
        $this->labelBuilder->setText('Test Label');
        $label = $this->labelBuilder->getLabel()->getText();

        $this->assertEquals('Test Label', $label);
    }
}
