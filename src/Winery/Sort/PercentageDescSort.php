<?php

declare(strict_types=1);

namespace App\Winery\Sort;

class PercentageDescSort implements SortInterface
{
    public function sort(array $data): array
    {
        usort($data, function($item1, $item2) {
            return $item2['percentage'] <=> $item1['percentage'] ?: strcmp($item1['grape'], $item2['grape']);
        });

        return $data;
    }
}
