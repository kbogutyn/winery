<?php

declare(strict_types=1);

namespace App\Winery\Sort;

interface SortInterface
{
    public function sort(array $data): array;
}
