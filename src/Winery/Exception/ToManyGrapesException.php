<?php

declare(strict_types=1);

namespace App\Winery\Exception;

class ToManyGrapesException extends \RuntimeException implements WineryException
{
    public function __construct()
    {
        parent::__construct('To many grapes. No more than four types of grape are allowed in a wine label.');
    }
}