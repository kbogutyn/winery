<?php

declare(strict_types=1);

namespace App\Winery\Exception;

class LabelToLongException extends \RuntimeException implements WineryException
{
    public function __construct()
    {
        parent::__construct('Label is to long. Total label length must not be greater than 50 characters.');
    }
}