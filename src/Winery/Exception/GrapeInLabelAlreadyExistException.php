<?php

declare(strict_types=1);

namespace App\Winery\Exception;

class GrapeInLabelAlreadyExistException extends \RuntimeException implements WineryException
{
    public function __construct()
    {
        parent::__construct('Grape in Label already exist. Grape names must be unique on each label.');
    }
}