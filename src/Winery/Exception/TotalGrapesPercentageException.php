<?php

declare(strict_types=1);

namespace App\Winery\Exception;

class TotalGrapesPercentageException extends \RuntimeException implements WineryException
{
    public function __construct()
    {
        parent::__construct('Total grapes percentage could not be over one hundred percent.');
    }
}