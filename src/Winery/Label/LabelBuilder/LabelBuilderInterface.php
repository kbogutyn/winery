<?php

declare(strict_types=1);

namespace App\Winery\Label\LabelBuilder;

use App\Winery\Label\Label;

interface LabelBuilderInterface
{
    public function getLabel(): Label;

    public function setText(string $text): void;
}
