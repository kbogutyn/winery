<?php

declare(strict_types=1);

namespace App\Winery\Label\LabelBuilder;

use App\Winery\Exception\LabelToLongException;
use App\Winery\Label\Label;

class LabelBuilder implements LabelBuilderInterface
{
    private $label = null;

    public function __construct()
    {
        $this->label = new Label();
    }

    public function setText(string $text): void
    {
        if (strlen($text) > 50) {
            throw new LabelToLongException();
        }

        $this->label->setText($text);
    }

    public function getLabel(): Label
    {
        return $this->label;
    }
}
