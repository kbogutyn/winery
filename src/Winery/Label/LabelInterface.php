<?php

namespace App\Winery\Label;

interface LabelInterface
{
    public function getText(): ?string;

    public function setText(string $text): void;
}
