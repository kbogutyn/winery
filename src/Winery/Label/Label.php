<?php

declare(strict_types=1);

namespace App\Winery\Label;

class Label implements LabelInterface
{
    private $text;

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): void
    {
        $this->text = $text;
    }
}
