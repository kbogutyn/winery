<?php

declare(strict_types=1);

namespace App\Winery\Label\LabelDirector;

use App\Winery\Label\LabelBuilder\LabelBuilderInterface;
use App\Winery\Sort\SortInterface;

abstract class AbstractLabelDirector
{
    abstract public function __construct(LabelBuilderInterface $builder, SortInterface $sortType);

    abstract public function buildLabel();

    abstract public function getLabel();
}