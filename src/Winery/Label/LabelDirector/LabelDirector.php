<?php

declare(strict_types=1);

namespace App\Winery\Label\LabelDirector;

use App\Winery\Exception\GrapeInLabelAlreadyExistException;
use App\Winery\Exception\TotalGrapesPercentageException;
use App\Winery\Label\Label;
use App\Winery\Label\LabelBuilder\LabelBuilderInterface;
use App\Winery\Exception\ToManyGrapesException;
use App\Winery\Grape\GrapeInterface;
use App\Winery\Sort\SortInterface;

class LabelDirector extends AbstractLabelDirector
{
    private $builder;

    private $sortType;

    private $grapes = [];

    public function __construct(LabelBuilderInterface $builder, SortInterface $sortType)
    {
        $this->builder = $builder;
        $this->sortType = $sortType;
    }

    public function buildLabel(): void
    {
        $sorted = $this->sortType->sort($this->grapes);

        $label = '';
        foreach ($sorted as $grape) {
            $label .= ' '.$grape['grape'];
        }

        $this->builder->setText(trim($label));
    }

    public function getLabel(): Label
    {
        return $this->builder->getLabel();
    }

    public function addGrapeWithPercentage(GrapeInterface $grape, int $percentage): void
    {
        if (count($this->grapes) === 4) {
            throw new ToManyGrapesException();
        }

        if ($this->isGrapeAlreadyExist($grape)) {
            throw new GrapeInLabelAlreadyExistException();
        }

        if ($this->isTotalPercentageToHigh($percentage)) {
            throw new TotalGrapesPercentageException();
        }

        $this->grapes[] = ['grape' => $grape->getName(), 'percentage' => $percentage];
    }

    private function isGrapeAlreadyExist(GrapeInterface $grape): bool
    {
        foreach ($this->grapes as $val) {
            if ($val['grape'] === $grape->getName()) {
                return true;
            }
        }

        return false;
    }

    private function isTotalPercentageToHigh(int $percentage): bool
    {
        $total = 0;

        foreach ($this->grapes as $val) {
            $total += $val['percentage'];
        }

        return $total + $percentage > 100;
    }
}
