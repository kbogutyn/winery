<?php

namespace App\Winery\Grape;

class CabernetSauvignon implements GrapeInterface
{
    public function getName(): string
    {
        return 'Cabernet Sauvignon';
    }
}
