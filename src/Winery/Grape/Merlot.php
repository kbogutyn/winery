<?php

namespace App\Winery\Grape;

class Merlot implements GrapeInterface
{
    public function getName(): string
    {
        return 'Merlot';
    }
}
