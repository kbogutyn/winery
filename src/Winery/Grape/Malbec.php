<?php

namespace App\Winery\Grape;

class Malbec implements GrapeInterface
{
    public function getName(): string
    {
        return 'Malbec';
    }
}
