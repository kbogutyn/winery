<?php

namespace App\Winery\Grape;

class CabernetFranc implements GrapeInterface
{
    public function getName(): string
    {
        return 'Cabernet Franc';
    }
}
