<?php

declare(strict_types=1);

namespace App\Winery\Grape;

interface GrapeInterface
{
    public function getName(): string;
}
